package com.example.forms;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

public class SecondActivity extends AppCompatActivity {
    private TextView textView;
    private String userAge;
    private String radioButtonSelection;
    private RadioButton radioButtonFarewell;
    private Button button;
    private int counter = 0;



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SeekBar seekBar;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        textView = findViewById(R.id.textViewAgeSelected);
        seekBar = findViewById(R.id.seekBarSecondActivity);
        button = findViewById(R.id.buttonSecondActivity);
        radioButtonFarewell = findViewById(R.id.radioButtonFarewell);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userAge.isEmpty()) {
                    Toast.makeText(SecondActivity.this, "La edad tiene estar comprendida entre 18 y 60", Toast.LENGTH_LONG).show();
                } else {

                    if (radioButtonFarewell.isChecked()) {
                        radioButtonSelection = "farewell";
                    } else {
                        radioButtonSelection = "welcome";
                    }

                    String userName = getIntent().getStringExtra("userName");
                    Intent i = new Intent(SecondActivity.this, ThirdActivity.class);
                    i.putExtra("radioButtonSelection", radioButtonSelection);
                    i.putExtra("userName", userName);
                    i.putExtra("userAge", userAge);
                    startActivity(i);

                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBar.setProgress(progress);
                String age = "" + progress;
                textView.setText(age);
                userAge = age;
                int ageControl = Integer.parseInt(userAge);

                if (ageControl < 18 || ageControl > 60){
                    button.setVisibility(View.INVISIBLE);
                    counter++;
                    if (counter == 1){
                        Toast.makeText(SecondActivity.this,"La edad tiene que estar comprendida entre 18 y 60",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    button.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}