package com.example.forms;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ThirdActivity extends AppCompatActivity {
    private Button showButton;
    private Button shareButton;
    private String message="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        showButton = findViewById(R.id.buttonShowInfo);
        shareButton = findViewById(R.id.buttonShare);


        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = getIntent().getStringExtra("userName");
                String userAge = getIntent().getStringExtra("userAge");
                String selection = getIntent().getStringExtra("radioButtonSelection");
                int olderAge = Integer.parseInt(userAge) + 1;

                String textWelcome = "Hola " + userName + ", cómo llevas estos " + userAge + " años?";
                String textFarewell = "Espero volverte a ver " + userName + ", antes de que hagas " + olderAge + " años";

                assert selection != null;

                switch (selection){
                    case "welcome":
                        Toast.makeText(ThirdActivity.this, textWelcome, Toast.LENGTH_LONG).show();
                        message = textWelcome;
                        break;
                    case "farewell":
                        Toast.makeText(ThirdActivity.this, textFarewell, Toast.LENGTH_LONG).show();
                        message = textFarewell;
                        break;
                }

                showButton.setVisibility(View.GONE);
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (message.equals("")){
                    Toast.makeText(ThirdActivity.this, "No hay nada que compartir", Toast.LENGTH_LONG).show();
                }else {
                    Intent tweet = new Intent(Intent.ACTION_VIEW);
                    tweet.setData(Uri.parse("http://twitter.com/?status=" + Uri.encode(message)));//where message is your string message
                    startActivity(tweet);
                }


            }
        });

    }
}