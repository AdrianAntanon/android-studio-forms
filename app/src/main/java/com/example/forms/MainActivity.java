package com.example.forms;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private EditText editText;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        button = findViewById(R.id.buttonMainActivity);
        editText = findViewById(R.id.editTextName);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = editText.getText().toString();

                if (userName.isEmpty()){
                    Toast.makeText(MainActivity.this, "Introduce un nombre, por favor", Toast.LENGTH_LONG).show();

                }else {
                    Intent i = new Intent(MainActivity.this, SecondActivity.class);
                    i.putExtra("userName",userName);
                    startActivity(i);
                }
            }
        });
    }
}